/////////////////////////
//////UTILISATION////////
/////////////////////////

Pour tester le programme, exécuter dans l'orde les programme suivant : commande.php, gestionCuisinier.php, gestionGerant.php
Puis reset.php pour réinitialiser les fichiers et pouvoir recommencer la démonstration.


/////////////////////////
//////////DATA///////////
/////////////////////////

Nous avons utilisé pour nos data des fichiers .json, ces données sont reparties dans 6 fichiers + 1 dossier stats :
-> boisson.json : va stocker les boissons du restaurant

-> ingredient.json : va stocker tous les ingrédients que le cuisinier aura utiliser au fil du temps.
Ces ingrédients auront toujours un prix qui est déterminé par une référence (unité) : pièce, gramme, kg, millilitre..,
ce qui permettra au cuisinier de déterminer le coup de ses repas.

-> menu.json : va stocker tous les menus du restaurant (menu du jour || ou pas) avec :
- un prix
- un type de repas (si c'est un menu pour : des pâtes, pizzas..)
- si le menu contient une boisson, un dessert
- si le menu est un menu du jour (via l'attribut : "activate")

-> recette.json : va stocker l'ensemble des recettes ajouté par le cuisinier avec :
- un attribut "available" pour la disponibilité de la recette
- un temps et un niveau pour la réalisation de la recette
- un attribut "madeBy" qui contiendra le nom du cuisinier qui l'a créé
- chaque recette aura un parent qui définira son type de repas (pizza, pate..)

-> user.json : va stocker l'ensemble des utilisateurs de l'application avec :
- leur nom (en clé)
- leur mot de passe
- leur rôle dans le restaurant (cuisinier, serveur, gérant..)

-> history.json : va stocker un historique comportant toutes les commandes passé au restaurant avec :
- le nombre de repas
- le prix total de la commande
- le serveur qui a prit la commande
- le cuisinier qui a fait la commande
- la date de la commande
- la liste des repas + boissons de la commande

-> ::DOSSIER : stats : va stocker 4 fichiers json : day.json, month.json, week.json, year.json
Ces fichiers sont composé de :
- un nombre de commande
- un  nombre de menu
- un nombre de couvert
- une date (en KEY)


/////////////////////////
///////APPLICATION///////
/////////////////////////

//commande.php
-> Va simuler la prise d'une commande par un serveur puis l'envoi en cuisine,
une fois que le cuisinier a fini TOUS les repas de la commande il renverra un
message pour informer le serveur (tout ceci se fait en simulation via des echos).
Le cuisinier renverra aussi un message à chaque fois qu'il aura fini un repas dans
une commande, pour informer le serveur de son avancement dans la commande.

//gestionCuisinier.php
-> Va simuler une interaction entre le cuisinier et l'application pour voir les menus (menus du jour || tous les menus).
Puis va permettre au cuisinier d'ajouter un menu ou d'en supprimer un (dans cet exemple le cuisinier supprimera un menu).
PS: exécuter par la suite le fichier "reset.php" qui réinitialisera les menus supprimer (car le menu sera vraiment
supprimé du fichier menu.json sui contient la liste des menus)

//reset.php
-> Comme énoncé ci-dessus, le fichier reset.php va réinitialiser tous les fichiers .json étant été modifié (ou pas) précédemment.
Ceci se fait via un dossier "save" dans le dossier "data" -> une copie des fichiers .json du dossier "/data/save" seront
ré-enregistré dans le dossier "/data" pour remettre à jour les données (fichier .json et dossier "stats" avec son contenu).

//gestionGerant.php
-> Va simuler une interaction entre le gérant et l'application. Dans cet exemple le gérant se connectera avec le compte "thomas"
Le gérant va ensuite avoir le choix de visualiser l'historique de commande ou de voir les statistiques du restaurant, il fera les deux :
- premièrement il va voir l'historique de commande
- puis il va visualiser les statistiques du restaurant, il aura alors la possibilité de voir les stats : du jour, de la semaine,
du mois ou de l'année : dans notre exemple il sélectionnera les statistiques du mois


/////////////////////////
//////TEST UNITAIRE//////
/////////////////////////

Test unitaire fait avec phpunit (9.3.10);
Dossier /tests/

//HistoriqueTest.php
-> Va vérifier si une erreur est renvoyé pour la fonction getHistory qui prend en paramètre un limiteur (pour limiter le nombre
  de commande dans l'historique retourné) : on va ici entrer une chaîne de caractère à la place d'un entier;
  Le paramètre $limiter prend normalement que des entier plus grand que 0;

//MenuTest.php
-> Va vérifier si lorsqu'on utilise la fonction delete() de la classe Menu renvoie bien une erreur lorsqu'on essaie de supprimer
  un menu inexistant.
