<?php

const URL_DIR = "./data/";
const URL_SAVE_DIR = "./data/save/";

try{
  //COPIER LES FICHIERS DU DOSSIER SAVE + LES FICHIERS DE SES SOUS DOSSIERS (si il y'en a)
  $savedFilesTab = array_diff(scandir(URL_SAVE_DIR), array('..', '.'));
  foreach ($savedFilesTab as $v) {
    if(!is_dir(URL_SAVE_DIR.$v)){
      copy(URL_SAVE_DIR.$v, URL_DIR.$v);
    }
    else{
      $savedSousDirectoryFilesTab = array_diff(scandir(URL_SAVE_DIR.$v.'/'), array('..', '.'));
      foreach ($savedSousDirectoryFilesTab as $f) {
        if(!is_dir(URL_SAVE_DIR.$v.'/'.$f))
          copy(URL_SAVE_DIR.$v.'/'.$f, URL_DIR.$v.'/'.$f);
      }
    }
  }
  echo "Les fichiers et dossiers ont été réintialisés";
}
catch(Exception $e){
  $e->getMessage();
}
