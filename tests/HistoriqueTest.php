<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once './classes/gestion/Gestion.php';
require_once './classes/gestion/Historique.php';

final class HistoryTest extends TestCase
{
  public function testCanGetHistoryWithValidLimiter() : void
  {
    $history = new \classes\gestion\Historique();
    $this->assertEquals(
      "Le limiter entré n'est pas valide.\n",
      $history->getHistory("badLimiter")
    );
  }
}
