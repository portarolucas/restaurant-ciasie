<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require_once './classes/gestion/Gestion.php';
require_once './classes/gestion/Menu.php';

final class MenuTest extends TestCase
{
  public function testCanDeleteFromExistingMenu() : void
  {
    $menu = new \classes\gestion\Menu(-1);
    $this->assertEquals(
      "On ne peut pas supprimer un menu n'ayant pas été ajouté dans la table des menus ou étant inexistant.\n",
      $menu->delete()
    );
  }
}
