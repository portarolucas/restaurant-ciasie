<?php

require_once('classes/gestion/Gestion.php');
require_once('classes/gestion/Menu.php');
require_once('classes/user/Authentification.php');
require_once('classes/user/User.php');
require_once('classes/user/Cuisinier.php');

use classes\user\Authentification;

$userJson = file_get_contents("./data/user.json");

$auth = new Authentification($userJson);
try{
  $cuisinier = $auth->login("ozan", "unmdp2");
}
catch(\Exception $e){
  echo "Erreur cuisinier : ".$e->getMessage();
  exit(1);
}

//sleep(8);
$cuisinier->gestionCuisinier();
