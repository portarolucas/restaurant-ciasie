<?php

require_once('classes/user/Authentification.php');
require_once('classes/user/User.php');
require_once('classes/gestion/Gestion.php');

require_once('classes/user/Cuisinier.php');
require_once('classes/user/Serveur.php');
require_once('classes/gestion/Historique.php');
require_once('classes/gestion/Statistique.php');

use classes\user\Authentification;

$userJson = file_get_contents("./data/user.json");

$auth = new Authentification($userJson);
try{
  $serveur = $auth->login("lucas", "unmdp0");
}
catch(\Exception $e){
  echo "Erreur serveur : ".$e->getMessage();
  exit(1);
}
try{
  $cuisinier = $auth->login("ozan", "unmdp2");
}
catch(\Exception $e){
  echo "Erreur cuisinier : ".$e->getMessage();
  exit(1);
}

$command = $serveur->takeCommand();
//sleep(8);
$cuisinier->doCommand($command);
//sleep(5);
$serveur->collect($command, $cuisinier->nom);
