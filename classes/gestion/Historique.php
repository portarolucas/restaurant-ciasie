<?php

namespace classes\gestion;

class Historique extends Gestion{
  const HISTORY_URL_FILE = "./data/history.json";

  public static function updateHistory($command, $totalPrice, $serveur_name, $cuisinier_name){
    $historyTab = json_decode(file_get_contents(self::HISTORY_URL_FILE), true);

    $nb_repas = sizeof($command);
    $prix_total = $totalPrice;
    $serveur = $serveur_name;
    $cuisinier = $cuisinier_name;
    $date = date("d").'/'.date("m").'/'.date("Y");

    $repasTab = [];
    $repasID = 1;
    foreach ($command as $v) {
      $repasTab[++$repasID] = ["menu_id" => $v[1], "repas" => $v[2], "boisson_id" => $v[3]];
    }

    //on attribu au nouvel enregistrement le plus gros ID + 1 de la table history
    if(sizeof(array_keys($historyTab)) > 0)
      $tabIndex = max(array_keys($historyTab)) + 1;
    else
      $tabIndex = 1;
    $historyTab[$tabIndex] = ["nb_repas" => $nb_repas, "prix_total" => $prix_total, "serveur" => $serveur, "cuisinier" => $cuisinier, "date" => $date, "repas" => $repasTab];

    //fin mise à jour du fichier json
    self::editJson(json_encode($historyTab), self::HISTORY_URL_FILE);
  }

  public static function getHistory($limit){
    if(is_numeric($limit) && ($limit > 0)){
      $historyTab = json_decode(file_get_contents(self::HISTORY_URL_FILE), true);
      $onlyLastElem = array_slice($historyTab, -10, 10, TRUE);
      return $onlyLastElem;
    }
    else{
      return "Le limiter entré n'est pas valide.\n";
    }
  }
}
