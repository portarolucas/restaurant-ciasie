<?php

namespace classes\gestion;

class Menu extends Gestion{
  private $id;
  private $menuTab;

  public $name;
  public $type_repas;
  public $hasBoisson;
  public $hasDessert;
  public $price;
  public $activate;

  const MENU_URL_FILE = "./data/menu.json";

  public function __construct($_id = null){
    $this->menuTab = json_decode(file_get_contents(self::MENU_URL_FILE), true);
    $this->id = $_id;
    if($_id && $this->menuTab[$_id]){
      $this->name = $this->menuTab[$this->id]["name"];
      $this->type_repas = $this->menuTab[$this->id]["type_repas"];
      $this->hasBoisson = $this->menuTab[$this->id]["boisson"];
      $this->hasDessert = $this->menuTab[$this->id]["dessert"];
      $this->price = $this->menuTab[$this->id]["price"];
      $this->activate = $this->menuTab[$this->id]["activate"];
    }
  }

  public function save(){
    //Si le menu n'existe pas encore il n'a pas d'id -> il faut donc lui en attribuer un
    //On pourra modifier un menu via les attributs de sa classe puis sauvegarder pour modifier le fichier menu.json
    //On pourra créer directement un nouveau menu en instanciant une nouvelle classe Menu puis utiliser cette méthode
    //pour l'ajouter dans le fichier menu.json, on lui attribura le menu avec le plus gros ID + 1
    if(!$this->id){
      $tabHigherIndex = max(array_keys($menuTab));
      $this->id = $tabHigherIndex++;
    }
    $this->menuTab[$this->id]["name"] = $this->name;
    $this->menuTab[$this->id]["type_repas"] = $this->type_repas;
    $this->menuTab[$this->id]["boisson"] = $this->hasBoisson;
    $this->menuTab[$this->id]["dessert"] = $this->hasDessert;
    $this->menuTab[$this->id]["price"] = $this->price;
    $this->menuTab[$this->id]["activate"] = $this->activate;
    $this->editJson(json_encode($this->menuTab[$this->id]), self::MENU_URL_FILE);
  }

  //test unitaire
  public function delete(){
    if($this->id){
      if(isset($this->menuTab[$this->id])){
        $name = $this->menuTab[$this->id]["name"];
        unset($this->menuTab[$this->id]);
        $this->editJson(json_encode($this->menuTab), self::MENU_URL_FILE);
        return "\e[0;34;47mLe menu {$name} (id: {$this->id}) a bien été supprimé."."\e[0m\n";
      }
      else{
        return "On ne peut pas supprimer un menu n'ayant pas été ajouté dans la table des menus ou étant inexistant.\n";
      }
    }else{
      return "Aucun ID n'est spécifié pour le menu. Surement que le menu n'a pas encore été enregistré.";
    }
  }
}
