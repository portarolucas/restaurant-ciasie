<?php

namespace classes\gestion;

class Statistique extends Gestion{

  const STAT_DAY_URL_FILE = "./data/stats/day.json";
  const STAT_WEEK_URL_FILE = "./data/stats/week.json";
  const STAT_MONTH_URL_FILE = "./data/stats/month.json";
  const STAT_YEAR_URL_FILE = "./data/stats/year.json";

  public static function updateStats($nbMenu, $nbRepas){
    $statsDayTab = json_decode(file_get_contents(self::STAT_DAY_URL_FILE), true);
    $statsWeekTab = json_decode(file_get_contents(self::STAT_WEEK_URL_FILE), true);
    $statsMonthTab = json_decode(file_get_contents(self::STAT_MONTH_URL_FILE), true);
    $statsYearTab = json_decode(file_get_contents(self::STAT_YEAR_URL_FILE), true);

    $todayFullDate = date("d").'/'.date("m").'/'.date("Y");
    $weekDate = date("W");
    $monthYearDate = date("m").'/'.date("Y");
    $yearDate = date("Y");

    //DAY STATS
    if($statsDayTab[$todayFullDate]){
      $statsDayTab[$todayFullDate]["nb_commande"]++;
      $statsDayTab[$todayFullDate]["nb_menu"] += $nbMenu;
      $statsDayTab[$todayFullDate]["nb_couvert"] += $nbRepas;
    }else{
      $statsDayTab[$todayFullDate]["nb_commande"] = 1;
      $statsDayTab[$todayFullDate]["nb_menu"] = $nbMenu;
      $statsDayTab[$todayFullDate]["nb_couvert"] = $nbRepas;
    }
    //WEEK STATS
    if($statsWeekTab[$weekDate]){
      $statsWeekTab[$weekDate]["nb_commande"]++;
      $statsWeekTab[$weekDate]["nb_menu"] += $nbMenu;
      $statsWeekTab[$weekDate]["nb_couvert"] += $nbRepas;
    }else{
      $statsWeekTab[$weekDate]["nb_commande"] = 1;
      $statsWeekTab[$weekDate]["nb_menu"] = $nbMenu;
      $statsWeekTab[$weekDate]["nb_couvert"] = $nbRepas;
    }
    //MONTH STATS
    if($statsMonthTab[$monthYearDate]){
      $statsMonthTab[$monthYearDate]["nb_commande"]++;
      $statsMonthTab[$monthYearDate]["nb_menu"] += $nbMenu;
      $statsMonthTab[$monthYearDate]["nb_couvert"] += $nbRepas;
    }else{
      $statsMonthTab[$monthYearDate]["nb_commande"] = 1;
      $statsMonthTab[$monthYearDate]["nb_menu"] = $nbMenu;
      $statsMonthTab[$monthYearDate]["nb_couvert"] = $nbRepas;
    }
    //YEAR STATS
    if($statsYearTab[$yearDate]){
      $statsYearTab[$yearDate]["nb_commande"]++;
      $statsYearTab[$yearDate]["nb_menu"] += $nbMenu;
      $statsYearTab[$yearDate]["nb_couvert"] += $nbRepas;
    }else{
      $statsYearTab[$yearDate]["nb_commande"] = 1;
      $statsYearTab[$yearDate]["nb_menu"] = $nbMenu;
      $statsYearTab[$yearDate]["nb_couvert"] = $nbRepas;
    }
    self::editJson(json_encode($statsDayTab), self::STAT_DAY_URL_FILE);
    self::editJson(json_encode($statsWeekTab), self::STAT_WEEK_URL_FILE);
    self::editJson(json_encode($statsMonthTab), self::STAT_MONTH_URL_FILE);
    self::editJson(json_encode($statsYearTab), self::STAT_YEAR_URL_FILE);
  }

  public static function getStats($periode, $date){
    if($periode == 'byDay'){
      $statsDayTab = json_decode(file_get_contents(self::STAT_DAY_URL_FILE), true);
      return $statsDayTab[$date];
    }else if($periode == 'byWeek'){
      $statsWeekTab = json_decode(file_get_contents(self::STAT_WEEK_URL_FILE), true);
      return $statsWeekTab[$date];
    }else if($periode == 'byMonth'){
      $statsMonthTab = json_decode(file_get_contents(self::STAT_MONTH_URL_FILE), true);
      return $statsMonthTab[$date];
    }else if($periode == 'byYear'){
      $statsYearTab = json_decode(file_get_contents(self::STAT_YEAR_URL_FILE), true);
      return $statsYearTab[$date];
    }
  }
}
