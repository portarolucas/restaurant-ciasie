<?php

namespace classes\user;

class Authentification{

  private $userTab;

  public function __construct($json_user){
    $this->userTab = json_decode($json_user, true);
  }

  public function login($username, $password){
    $username = strtolower($username);
    if($this->userTab[$username]){
      $userInfo = $this->userTab[$username];
      if($userInfo["password"] == $password){
        if($userInfo["role"] == "Serveur")
          return (new Serveur($username));
        else if($userInfo["role"] == "Cusinier")
          return (new Cuisinier($username));
        else if($userInfo["role"] == "Gerant")
          return (new Gerant($username));
      }
      else
        throw new \Exception("Mot de passe inccorect", 1);
    }
    else{
      throw new \Exception("Le nom d'utilisateur n'existe pas.", 1);
    }
  }
}
