<?php

namespace classes\user;

class Gerant extends User{

  public function __construct($_nom){
      parent::__construct($_nom);
  }

  public function welcomeMessage(){
    echo "Bienvenue ".$this->nom."\n";
    echo "Connecté en tant que : gérant\n";
  }

  //test unitaire
  public function showHistory(){
    $limiterCommand = 10;
    $history = \classes\gestion\Historique::getHistory($limiterCommand);
    if($history != "Le limiter entré n'est pas valide."){
      echo "------------------------"."\n";
      echo "Voici l'historique de commande ({$limiterCommand} dernière)' :\n";
      foreach ($history as $key => $v) {
        $nb_repas = $v["nb_repas"];
        $prix_total = $v["prix_total"];
        $serveur = $v["serveur"];
        $cuisinier = $v["cuisinier"];
        $date = $v["date"];
        $listRepas = $v["repas"];

        echo "------------------------"."\n";
        echo "| Commande ID : {$key}\n";
        echo "- Nombre de repas : {$nb_repas}\n";
        echo "- Serveur : {$serveur}\n";
        echo "- Cuisinier : {$cuisinier}\n";
        echo "- Date : {$date}\n";
        echo "| Listes des repas :\n";
        foreach ($listRepas as $repas) {
          $boisson = $this->findBoissonByID($repas["boisson_id"]);
          if(isset($boisson["name"]) && $boisson["name"])
            $boissonName = $boisson["name"];
          else
            $boissonName = "Aucune";
          echo "- Repas : ".$repas["repas"]." | Boisson : ".$boissonName."\n";
        }
      }
    }
    else
      echo $history;
  }

  public function showStats($periode){
    if($periode == 'byDay'){
      $stats = \classes\gestion\Statistique::getStats($periode, date("d").'/'.date("m").'/'.date("Y"));
      echo "------------------------"."\n";
      echo "Voici les statistiques du jour :\n";
      echo "- Nombre de commande : ".$stats["nb_commande"]."\n";
      echo "- Nombre de menu : ".$stats["nb_menu"]."\n";
      echo "- Nombre de couvert : ".$stats["nb_couvert"]."\n";
    }else if($periode == 'byWeek'){
      $stats = \classes\gestion\Statistique::getStats($periode, date("W"));
      echo "------------------------"."\n";
      echo "Voici les statistiques de la semaine :\n";
      echo "- Nombre de commande : ".$stats["nb_commande"]."\n";
      echo "- Nombre de menu : ".$stats["nb_menu"]."\n";
      echo "- Nombre de couvert : ".$stats["nb_couvert"]."\n";
    }else if($periode == 'byMonth'){
      $stats = \classes\gestion\Statistique::getStats($periode, date("m").'/'.date("Y"));
      echo "------------------------"."\n";
      echo "Voici les statistiques du mois :\n";
      echo "- Nombre de commande : ".$stats["nb_commande"]."\n";
      echo "- Nombre de menu : ".$stats["nb_menu"]."\n";
      echo "- Nombre de couvert : ".$stats["nb_couvert"]."\n";
    }else if($periode == 'byYear'){
      $stats = \classes\gestion\Statistique::getStats($periode, date("Y"));
      echo "------------------------"."\n";
      echo "Voici les statistiques de l'année :\n";
      echo "- Nombre de commande : ".$stats["nb_commande"]."\n";
      echo "- Nombre de menu : ".$stats["nb_menu"]."\n";
      echo "- Nombre de couvert : ".$stats["nb_couvert"]."\n";
    }
  }

  public function gestionGerant(){
    $this->welcomeMessage();
    echo "------------------------"."\n";
    echo "Que voulez-vous faire ?"."\n";
    echo "1. Historique de commande"."\n";
    echo "2. Statistiques du restaurant"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 1"."\e[0m\n";
    sleep(4);
    $this->showHistory();
    sleep(8);
    echo "------------------------"."\n";
    echo "Que voulez-vous faire ?"."\n";
    echo "1. Retour"."\n";
    echo "2. Quitter le programme"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 1"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";
    echo "Que voulez-vous faire ?"."\n";
    echo "1. Historique de commande"."\n";
    echo "2. Statistiques du restaurant"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 2"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";
    echo "Quel type de statistiques voulez vous voir ?"."\n";
    echo "1. Voir les statistiques du jour"."\n";
    echo "2. Voir les statistiques de la semaine"."\n";
    echo "3. Voir les statistiques du mois"."\n";
    echo "4. Voir les statistiques de l'année"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 3"."\e[0m\n";
    sleep(4);
    $this->showStats('byMonth');
    sleep(8);
  }

}
