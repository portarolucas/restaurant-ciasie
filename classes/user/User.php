<?php

namespace classes\user;

abstract class User{
  public $nom;

  private $boissonTab;
  private $ingredientTab;
  private $menuTab;
  private $recetteTab;

  public function __construct($_nom){
    $this->nom = $_nom;
    $this->boissonTab = json_decode(file_get_contents("./data/boisson.json"), true);
    $this->ingredientTab = json_decode(file_get_contents("./data/ingredient.json"), true);
    $this->menuTab = json_decode(file_get_contents("./data/menu.json"), true);
    $this->recetteTab = json_decode(file_get_contents("./data/recette.json"), true);
  }

  public function __set($property, $value)
  {
    $this->{$property} = $value;
  }

  public function __get($property)
  {
    return $this->{$property};
  }

  abstract public function welcomeMessage();

  public function findBoissonByID($id){
    return $this->boissonTab["id"];
  }

}
