<?php

namespace classes\user;

class Cuisinier extends User{

  /*
  $preparationTable:
  $key -> numéro de table
  [0] -> nombre de repas à faire pour cette table
  [1] -> nombre de repas terminé pour cette table
  */
  public $preparationTable = array();

  public function __construct($_nom){
      parent::__construct($_nom);
  }

  public function welcomeMessage(){
    echo "Bienvenue ".$this->nom."\n";
    echo "Connecté en tant que : cuisinier\n";
  }

  public function preparedCommand($repas){
    $tableNum = $repas[0];
    $repasName = $repas[2];
    $this->preparationTable[$tableNum][1]++;
    echo "------------------------"."\n";
    echo "\e[0;34;47mLe cuisinier viens de finir le repas : ".$repasName." pour la table numéro : ".$tableNum."\e[0m\n";
    if($this->preparationTable[$tableNum][0] == $this->preparationTable[$tableNum][1]){
      echo "------------------------"."\n";
      echo "------------------------"."\n";
      echo "\e[0;31;42mLe cuisinier a fini la commande pour la table numéro : ".$tableNum."\e[0m\n";
      echo "Le serveur peut venir la chercher.\n";
    }
  }

  public function doCommand($command){
    echo "------------------------"."\n";
    echo "Le cuisinier ".$this->nom." a bien reçu la commande est la commence.\n";
    sleep(4);
    if(sizeof($command) > 0){
      $this->preparationTable[$command[0][0]] = [sizeof($command), 0];
      foreach ($command as $key => $v) {
        $this->preparedCommand($v);
        sleep(8);
      }
    }
  }

  public function showMenu($onlyDay = false){
    //$onlyDay == voir seulement les menus du jour
    if($onlyDay){
      foreach ($this->menuTab as $key => $v) {
        if($v["activate"]){
          echo "ID: {$key} - ".$v["name"]."\n";
        }
      }
    }else{
      foreach ($this->menuTab as $key => $v) {
        echo "ID: {$key} - ".$v["name"].(($v["activate"]) ? ' (est un menu du jour)' : '')."\n";
      }
    }
  }

  public function showPrimaryChoices($exempleValue){
    echo "1. Visualiser tous les menus";
    echo "2. Visualiser les menus du jour uniquement";
    echo "3. Visualiser la liste des recettes";
    return $exempleValue;
  }

  public function gestionCuisinier(){
    $this->welcomeMessage();
    echo "------------------------"."\n";
    echo "Que voulez-vous faire ?"."\n";
    echo "1. Visualiser tous les menus"."\n";
    echo "2. Visualiser les menus du jour uniquement"."\n";
    echo "3. Visualiser la liste des recettes"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 2"."\e[0m\n";
    sleep(4);
    $this->showMenu(true);
    echo "------------------------"."\n";
    echo "Que voulez-vous faire ?"."\n";
    echo "1. Ajouter un menu"."\n";
    echo "2. Retirer un menu"."\n";
    echo "3. Revenir à l'accueil (Quitter)"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> 2"."\e[0m\n";
    sleep(4);
    echo "Veuillez entrer l'id d'un menu afin de le retirer :"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez entré l'id 15."."\e[0m\n";
    sleep(4);
    $menu = new \classes\gestion\Menu(15);
    echo $menu->delete();
    echo "------------------------"."\n";

  }
}
