<?php

namespace classes\user;

class Serveur extends User{

  public function __construct($_nom){
      parent::__construct($_nom);
  }

  public function collect($command, $cuisinier_name){
    $tableNum = $command[0][0];
    echo "------------------------"."\n";
    echo "Encaisser la commande pour la table numéro : {$tableNum}\n";
    sleep(4);
    //prix fictif (nombre de commande * 9.4 (prix au hasard)) + et qui ajoute ensuite la minute actuel au total
    //pour avoir des totals non identique
    $totalPrice = sizeof($command) * 9.4 + date('i');
    $nbMenu = sizeof($command);
    //l'application prend que des menus pour l'instant..
    //donc on peut attribuer à nbRepas la même valeur que le nombre de menu
    $nbRepas = $nbMenu;
    \classes\gestion\Statistique::updateStats($nbMenu, $nbRepas);
    \classes\gestion\Historique::updateHistory($command, $totalPrice, $this->nom, $cuisinier_name);
  }

  public function selectMenu($exempleValue) : int{
    foreach ($this->menuTab as $v) {
      echo $v["name"]."\n";
    }
    return $exempleValue;
  }

  public function selectRepas($exempleValue, $menu) : string{
    foreach ($this->recetteTab[$this->menuTab[$menu]["type_repas"]] as $key => $v) {
      echo $key."\n";
    }
    return $exempleValue;
  }

  public function selectBoisson($exempleValue) : int{
    foreach ($this->boissonTab as $v) {
      echo $v["name"]."\n";
    }
    return $exempleValue;
  }

  public function welcomeMessage(){
    echo "Bienvenue ".$this->nom."\n";
    echo "Connecté en tant que : serveur\n";
  }

  public function takeCommand(){
    $commandTable = array();
    $numeroTable = 2;
    $this->welcomeMessage();
    echo "------------------------"."\n";
    echo "Entrer le numéro de table :"."\n";
    echo "\e[0;31;42mVous avez sélectionné -> {$numeroTable}"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    /* REPAS 1 */
    /* ***************** */

    echo "Sélectionnez un menu dans la liste :"."\n";
    $menuClient = $this->selectMenu(2);
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> ".$this->menuTab[$menuClient]["name"]."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Veuillez sélectionner un repas :"."\n";
    $repasClient = $this->selectRepas("Carbonara", $menuClient);
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> {$repasClient}"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Veuillez sélectionner une boisson :"."\n";
    $boissonClient = $this->selectBoisson(2);
    sleep(4);
    echo "\e[0;31;42mVous avez sélectionné -> {$this->boissonTab[$boissonClient]['name']}"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Valider le repas ?\n";
    sleep(3);
    array_push($commandTable, [$numeroTable, $menuClient, $repasClient, $boissonClient]);
    echo "Vous avez validé le repas.\n";

    echo "Terminer la commande pour cette table ou prendre d'autre repas ?"."\n";
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné : Prendre d'autre repas\e[0m\n";

    echo "------------------------"."\n";
    sleep(4);

    /* REPAS 2 */
    /* ***************** */

    echo "Sélectionnez un menu dans la liste :"."\n";
    $menuClient2 = $this->selectMenu(0);
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> ".$this->menuTab[$menuClient2]["name"]."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Veuillez sélectionner un repas :"."\n";
    $repasClient2 = $this->selectRepas("Romaine", $menuClient2);
    sleep(3);
    echo "\e[0;31;42mVous avez sélectionné -> {$repasClient2}"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Veuillez sélectionner une boisson :"."\n";
    $boissonClient2 = $this->selectBoisson(1);
    sleep(4);
    echo "\e[0;31;42mVous avez sélectionné -> {$this->boissonTab[$boissonClient2]['name']}"."\e[0m\n";
    sleep(4);
    echo "------------------------"."\n";

    echo "Valider le repas ?\n";
    sleep(3);
    array_push($commandTable, [$numeroTable, $menuClient2, $repasClient2, $boissonClient2]);
    echo "Vous avez validé le repas.\n";

    echo "Terminer la commande pour cette table ou prendre d'autre repas ?"."\n";
    sleep(3);
    echo "\e[0;31;42mTerminer : la commande est passé en cuisine\e[0m\n";

    return $commandTable;//un tableau dans un tableau au cas si on a plusieurs commandes
  }

}
