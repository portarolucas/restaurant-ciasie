<?php

require_once('classes/user/Authentification.php');
require_once('classes/user/User.php');
require_once('classes/user/Gerant.php');

require_once('classes/gestion/Gestion.php');
require_once('classes/gestion/Historique.php');
require_once('classes/gestion/Statistique.php');

use classes\user\Authentification;

$userJson = file_get_contents("./data/user.json");

$auth = new Authentification($userJson);
try{
  $gerant = $auth->login("thomas", "unmdp3");
}
catch(\Exception $e){
  echo "Erreur cuisinier : ".$e->getMessage();
  exit(1);
}

//sleep(8);
$gerant->gestionGerant();
